<?php

namespace App\Console\Commands;

use App\Song;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdatePlaylist extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'playlist:update {mount}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks Ice Cast Server for Latest Playlist Info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $mount = $this->argument('mount');

        //Get Latest Play to Ensure we aren't duplicating
        $latestPlay = DB::table('playlist')->where('mount', $mount)->get()->last();

        //Get Now Playing Data
        $url = "https://ice.my80sstation.com/$mount.xspf";
        $xml = simplexml_load_file($url);
        $string = (string) $xml->trackList->track->title;
        $parts = explode(' - ', $string);
        $artist = ucwords($parts[0]);
        $title = ucwords(end($parts));

        $song = Song::firstOrCreate([
            'artist' => $artist,
            'title' => $title
        ]);

        //If the latest song isn't the currently playing song, it's a new song.
        if($latestPlay == null || $latestPlay->song_id != $song->id){
            DB::table('playlist')->insert([
                'mount' => $mount,
                'song_id' => $song->id,
                'played_at' => Carbon::now()
            ]);
        }

        Log::info("[Playlist Update] Updated {$mount}");

        return 0;
    }
}
