<?php

namespace App\Http\Controllers;

use App\Mail\OrderReceived;
use App\Mail\OrderReceivedReceipt;
use App\Merch;
use App\Purchase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class PurchaseController extends Controller
{
    public function buy(Merch $merch)
    {

        $paymentIntent = $this->createIntent($merch);

        return view('merch.buy', compact('merch', 'paymentIntent'));
    }

    public function submit(Request $request)
    {
        $purchase = new Purchase();
        $purchase->stripe_id = $request->input('data.paymentData.stripe_id');
        $purchase->stripe_payment_method = $request->input('data.paymentData.stripe_payment_method');
        $purchase->stripe_payment_method_name = $request->input('data.paymentData.stripe_payment_method_name');
        $purchase->charged_amount = $request->input('data.paymentData.charged_amount');
        $purchase->currency = $request->input('data.paymentData.currency');
        $purchase->merch_id = intval($request->input('data.purchaseData.merch_id'));
        $purchase->billing_name = $request->input('data.purchaseData.billing_name');
        $purchase->billing_street = $request->input('data.purchaseData.billing_street');
        $purchase->billing_city = $request->input('data.purchaseData.billing_city');
        $purchase->billing_state = $request->input('data.purchaseData.billing_state');
        $purchase->billing_zip = $request->input('data.purchaseData.billing_zip');
        $purchase->billing_email = $request->input('data.purchaseData.billing_email');
        $purchase->shipping_name = $request->input('data.purchaseData.shipping_name');
        $purchase->shipping_street = $request->input('data.purchaseData.shipping_street');
        $purchase->shipping_city = $request->input('data.purchaseData.shipping_city');
        $purchase->shipping_state = $request->input('data.purchaseData.shipping_state');
        $purchase->shipping_zip = $request->input('data.purchaseData.shipping_zip');
        $purchase->size = $request->input('data.purchaseData.size');

        $purchase->save();


        Mail::to(env('ORDER_EMAIL'))->send(new OrderReceived($purchase));
        Mail::to($purchase->billing_email)->send(new OrderReceivedReceipt($purchase));

        Session::put('order_successful', ['item_name' => $purchase->merch->name]);

        return response()->json(['success' => true, 'redirect_url' => url('/')]);

    }

    /**
     * @return PaymentIntent
     * @throws ApiErrorException
     */
    private function createIntent(Merch $merch)
    {

        Stripe::setApiKey(getenv('STRIPE_SECRET'));

        $price = $merch->price + $merch->shipping;
        try {
            return PaymentIntent::create([
                'amount' => $price,
                'currency' => 'usd',
                'description' => 'My80s.live Merchandise',
                'statement_descriptor' => "my80s",
                'statement_descriptor_suffix' => $merch->name,
                "payment_method_types" => [
                    "card",
                ]
            ]);
        } catch (ApiErrorException $e){
            Log::error("[STRIPE API ERROR] {$e->getMessage()}");
        }
    }

}
