<?php

namespace App\DashboardWidgets;

use App\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class Posts extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $posts = Post::whereNull('deleted_at')
                    ->count();
        
        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-file-text',
            'title'  => "{$posts} Posts",
            'text'   => "You have $posts Published Posts",
            'button' => [
                'text' => "View All Posts",
                'link' => url('/admin/posts'),
            ],
            
            'image' => asset('storage/settings/August2020/9QjTLSjuaQlAb53xgZit.jpg'),
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return true;
    }
}
