<?php

use App\Http\Controllers\PageController;
use App\Http\Controllers\PurchaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/', [PageController::class, 'home']);
Route::get('/{slug}', [PageController::class, 'post']);
//Route::get('/merch/{merch}', [PurchaseController::class, 'buy']);
Route::get('/merch/{merch}', function(){
    return 'Coming Soon! Again! :)';
});
//Route::post('/merch/submit', [PurchaseController::class, 'submit']);
