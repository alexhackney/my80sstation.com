//Sets Up Payment Request
let paymentRequest = stripe.paymentRequest({
    country: 'US',
    currency: 'usd',
    total: {
        label: paymentLabel,
        amount: paymentAmount,
    }
});

//Element Setup
let elements = stripe.elements({
    fonts: [
        {
            cssSrc: 'https://fonts.googleapis.com/css?family=Source+Code+Pro',
        },
    ],
    locale: 'auto'
});

//Element Styles
let elementStyles = {
    base: {
        color: '#32325D',
        fontWeight: 500,
        fontFamily: 'Source Code Pro, Consolas, Menlo, monospace',
        fontSize: '12px',
        fontSmoothing: 'antialiased',

        '::placeholder': {
            color: '#CFD7DF',
        },
        ':-webkit-autofill': {
            color: '#e39f48',
        },
    },
    invalid: {
        color: '#E25950',

        '::placeholder': {
            color: '#FFCCA5',
        },
    },
};

//Element Classes
let elementClasses = {
    focus: 'focused',
    empty: 'empty',
    invalid: 'invalid',
    base: 'form-control'
};

//Google Apple Pay Setup
let prButton = elements.create('paymentRequestButton', {
    paymentRequest: paymentRequest,
    style: {
        paymentRequestButton: {
            // One of 'default', 'book', 'buy', or 'donate'
            type: 'buy',
            // Defaults to '40px'. The width is always '100%'.
            height: '40px',
            // One of 'dark', 'light', or 'light-outline'
            theme: 'dark'
        },
    }
});

//Setup form for CC Payment
let cardNumber = elements.create('cardNumber', {
    style: elementStyles,
    classes: elementClasses,
});
let cardExpiry = elements.create('cardExpiry', {
    style: elementStyles,
    classes: elementClasses,
});
let cardCvc = elements.create('cardCvc', {
    style: elementStyles,
    classes: elementClasses,
});
let card = elements.create;
cardNumber.mount('#card-number');
cardExpiry.mount('#card-expiration');
cardCvc.mount('#card-cvc');


//If they can make a payment then mount the payment request buttons and form
paymentRequest.canMakePayment().then(function(result) {
    if (result) {
        //console.log('can make payment');
        //cardNumber.mount('#card-number');
        //cardExpiry.mount('#card-expiration');
        //cardCvc.mount('#card-cvc');
        //document.getElementById('display-card-form-btn').classList.remove('d-none');
        prButton.mount('#payment-request-button');
    } else {
        //console.log('can not make payment');
        document.getElementById('payment-request-button').style.display = 'none';
    }
});

paymentRequest.on('paymentmethod', function(ev) {
    // Confirm the PaymentIntent without handling potential next actions (yet).
    //console.log('on payment method');
    //console.log(ev);
    let paymentData = {
        stripe_payment_method_name: ev.methodName,
        stripe_payment_payer_email: ev.payerEmail,
        stripe_payment_payer_name: ev.payerName,
        stripe_payment_payer_phone: ev.payerPhone,
    };
    stripe.confirmCardPayment(
        paymentIntent,
        {payment_method: ev.paymentMethod.id},
        {handleActions: false}
    ).then(function(confirmResult) {
        if (confirmResult.error) {
            // Report to the browser that the payment failed, prompting it to
            // re-show the payment interface, or show an error message and close
            // the payment interface.
            ev.complete('fail');
            //console.log(ev.complete('fail'));
        } else {
            // Report to the browser that the confirmation was successful, prompting
            // it to close the browser payment method collection interface.
            //console.log('success');
            ev.complete('success');
            // Check if the PaymentIntent requires any actions and if so let Stripe.js
            // handle the flow. If using an API version older than "2019-02-11" instead
            // instead check for: `paymentIntent.status === "requires_source_action"`.
            if (confirmResult.paymentIntent.status === "requires_action") {
                // Let Stripe.js handle the rest of the payment flow.
                stripe.confirmCardPayment(clientSecret).then(function(result) {
                    if (result.error) {
                        // The payment failed -- ask your customer for a new payment method.
                    } else {
                        //console.log('payment succeeded after confirm');
                        // The payment has succeeded.
                    }
                });
            } else {
                paymentData.stripe_id = confirmResult.paymentIntent.id;
                paymentData.stripe_payment_method = confirmResult.paymentIntent.payment_method;
                paymentData.charged_amount = confirmResult.paymentIntent.amount;
                paymentData.currency = confirmResult.paymentIntent.currency;
                handleSuccessfulPayment(paymentData);
            }
        }
    });
});

//CC Payment Handling
let paymentSubmitButton = document.getElementById('payment-submit-btn');
let form = document.getElementById('merch-purch');
form.addEventListener('submit', function(ev)  {
    ev.preventDefault();
    let errorBox = document.getElementById('card-error-message');
    errorBox.classList.add('d-none');
    //console.log('Payment Form Submitted');
    paymentSubmitButton.disabled = true;
    let paymentMessage = document.getElementById('please-wait-msg');
    paymentMessage.classList.remove('d-none');
    // If the client secret was rendered server-side as a data-secret attribute
    // on the <form> element, you can retrieve it here by calling `form.dataset.secret`
    stripe.confirmCardPayment(paymentIntent, {
        payment_method: { card: cardNumber
        }
    }).then(function(result) {
        if (result.error) {
            // Show error to your customer (e.g., insufficient funds)
            //console.log(result.error.message);
            displayError(result.error.message);
            paymentSubmitButton.disabled = false;
            paymentMessage.classList.add('d-none');
        } else {
            // The payment has been processed!
            if (result.paymentIntent.status === 'succeeded') {
                //console.log('payment succeeded');
                //console.log(result);
                let paymentData = {
                    stripe_id: result.paymentIntent.id,
                    stripe_payment_method: result.paymentIntent.payment_method,
                    stripe_payment_method_name: 'card',
                    charged_amount: result.paymentIntent.amount,
                    currency: result.paymentIntent.currency,
                }
                handleSuccessfulPayment(paymentData);
                // Show a success message to your customer
                // There's a risk of the customer closing the window before callback
                // execution. Set up a webhook or plugin to listen for the
                // payment_intent.succeeded event that handles any business critical
                // post-payment actions.
            }
        }
    });
});

let cardForm = document.getElementById('card-form');
let cardFormDisplayButton = document.getElementById('display-card-form-btn');

let paymentRequestButton = document.getElementById('payment-request-button');
cardFormDisplayButton.addEventListener('click', ()=>{
    //console.log('showing form');
    cardForm.classList.remove('d-none');
    cardFormDisplayButton.classList.add('d-none');
    //paymentRequestButton.classList.add('d-none');
})

function handleSuccessfulPayment(paymentData) {

    let size = null;
    if(document.getElementById('size')){
         size = document.getElementById('size').value;
    }

    let data = {
        paymentData: paymentData,
        purchaseData: {
            merch_id: document.getElementById('merch_id').value,
            billing_name: document.getElementById('billing_name').value,
            billing_street: document.getElementById('billing_street').value,
            billing_city: document.getElementById('billing_city').value,
            billing_state: document.getElementById('billing_state').value,
            billing_zip: document.getElementById('billing_zip').value,
            billing_email: document.getElementById('billing_email').value,
            shipping_name: document.getElementById('shipping_name').value,
            shipping_street: document.getElementById('shipping_street').value,
            shipping_city: document.getElementById('shipping_city').value,
            shipping_state: document.getElementById('shipping_state').value,
            shipping_zip: document.getElementById('shipping_zip').value,
            size: size,
        },
    };

    //console.log(data);

    axios.post('/merch/submit', {
            data: data
        }).then(result => {
        if(result.data.success){
            if(result.data.analytics){

            }
            window.location.href = result.data.redirect_url;
        }
    });
}

function displayError(text){
    let errorBox = document.getElementById('card-error-message');
    errorBox.classList.remove('d-none');
    errorBox.innerHTML = '';
    errorBox.innerHTML = '<p class="small mb-0 pb-0">' + text + '</p>';
    //@todo Figure out a way to clear the error when they start to re input the info.
    let fields = document.getElementsByClassName('.InputElement');
    for (let i = 0; i < fields.length; i++) {
        fields[i].addEventListener('focus', ()=>{
            errorBox.classList.add('d-none');
        });
    }


}

