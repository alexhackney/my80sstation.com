<div style="text-align: center;
    background-color: #27a8e1;
    padding: 2em;
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
    border-radius: 20px;
    border: 1px solid black;
    box-shadow: 4px 4px 10px #012;">
    <h2>My 80's is partnering with <a href="https://anthrasyte.com/products/lovely-day-blend">Anthrasyte Coffee</a><br/>for a promotion for their new product, Lovely Day!</h2>
    <p><a href="https://anthrasyte.com/products/lovely-day-blend">Check out the new blend here.</a></p>
    <h2>Use promo code: <span style="font-weight:bolder;text-decoration: blink;">MY80s</span></h2>
    <div>
        <p><span style="font-weight:bold; font-size:.9em;"><a href="https://anthrasyte.com/products/lovely-day-blend">"LOVELY DAY" BLEND</a></span></p>
        <p style="font-size:.8em;">12 oz bag<br/>DESIGN: A balanced and lively daily cup accessible to all tastes.<br/>PROFILE: Balanced with plum and savory body.<br/>CURRENTLY: Ceiba, Guatemala; Serra Negra, Brazil The blend to start it all.</p>
    </div>
</div>
