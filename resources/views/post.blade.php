<!DOCTYPE html>
<html>
<head>
    <title>{{ $post->title }} | My 80s Station</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://use.typekit.net/hru4bkg.css">
    <style type="text/css">
        img{width: 90%;max-width:540px;margin:0 auto;display:block;}
        img.logo{width: 80%;max-width:400px;margin-bottom:2em;}
        p {text-align: center;font-size:1.6rem;font-family: trade-gothic-next, sans-serif;font-weight: 300;font-style: normal;}
        p.small {text-align: center;font-size:1.2rem;font-family: trade-gothic-next, sans-serif;font-weight: 300;font-style: normal;}
        p a {color:orange;text-decoration-line: none;}
        p a:hover {color:purple;text-decoration-line: none;}
        
        .blog-posts {font-family: trade-gothic-next, sans-serif; text-align:center; max-width:1024px; margin:0 auto; padding:1em;}
        .blog-posts h2 {margin-top:1em;font-size:2em;}
        .blog-posts h1 {font-size:2.4em;}
        .blog-posts h3 {font-size:1.8em;}
        .entry > a {font-size:1.6em; color:orange;text-decoration: none;}
        .entry > a:hover {color:red;}
        .entry > p {font-size:20px;}

    </style>
</head>
<body>
<div class="blog-posts">
    <h2>You Got Mel</h2>
    <h3>The 80s Blog</h3>
    <h1>{{ $post->title }}</h1>
    <p>Posted at {{ Carbon\Carbon::parse($post->created_at)->tz('America/New_York')->format('h:m a') }} on {{ Carbon\Carbon::parse($post->created_at)->tz('America/New_York')->format('m/d/Y') }}</p>
    <div class="entry">
        <p>{!! $post->body !!}</p>
        <p>>> My 80s Station <<</p>
        <p><a href="{{ url('/') }}">My80sStation.com</a></p>
    </div>
</div>
<footer>
<p>Got a comment, a Request?<br/>Let's hear it... <a href="mailto:hello@my80sstation.com" target="_blank">hello@my80sstation.com</a></p>
<p class="small">This website is being built now. Enjoy the music. Should work on any device.</p>
<p class="small">Trouble? <a href="mailto:admin@my80sstation.com" target="_blank">admin@my80sstation.com</a></p>
</footer>
</body>
</html>
