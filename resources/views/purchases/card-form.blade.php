<div class="row">
    <div class="col">
        <label class="form-label">Credit Card Number</label>
        <div class="form-group" id="card-number">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <label class="form-label">Expiration</label>
        <div class="form-group" id="card-expiration">
        </div>
    </div>
    <div class="col-6">
        <label class="form-label">CVC</label>
        <div class="form-group" id="card-cvc">
        </div>

    </div>
</div>
<div class="row">
    <div class="col">
        <div class="alert alert-danger text-center d-none" id="card-error-message"></div>
    </div>
</div>
