<p>
    <a href="https://my80s.live">
        <img style="text-align:center; max-width:200px;"
             src="{{ url('/images/my-80s-triangle-logo.png') }}" alt="My 80s" title="My 80s">
    </a>
</p>
<p>Thanks for your order!</p>

<p>We have received your order and are processing it now!</p>

<p>Item: {{ $purchase->merch->name }} @if($purchase->size != null) Size {{$purchase->size}} @endif</p>
<p>Charged Amount: ${{ $purchase->charged_amount/100 }}</p>
<p>Billing Info:<br/>
    {{$purchase->billing_name}}<br/>
    {{$purchase->billing_street}}<br/>
    {{$purchase->billing_city}}, {{$purchase->billing_state}} {{$purchase->billing_zip}}</p>
<p>Email: {{$purchase->billing_email}}</p>

<p>Shipping Info:<br/>
    @if($purchase->shipping_street == null)
        {{$purchase->billing_name}}<br/>
        {{$purchase->billing_street}}<br/>
        {{$purchase->billing_city}}, {{$purchase->billing_state}} {{$purchase->billing_zip}}</p>
    @else
        {{ $purchase->shipping_name }}<br/>
        {{ $purchase->shipping_street }}<br/>
        {{ $purchase->shipping_city }}, {{ $purchase->shipping_state }} {{ $purchase->shipping_zip }}</p>
    @endif

<p>If you have any questions, please let us know!</p>

