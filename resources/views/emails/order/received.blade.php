<p>Good news, you received a merch order!</p>

<p>Here are the details:</p>

<p>Item: {{ $purchase->merch->name }} @if($purchase->size != null) Size {{$purchase->size}} @endif</p>
<p>Charged Amount: ${{ $purchase->charged_amount/100 }}</p>
<p>Billing Info:<br/>
    {{$purchase->billing_name}}<br/>
    {{$purchase->billing_street}}<br/>
    {{$purchase->billing_city}}, {{$purchase->billing_state}} {{$purchase->billing_zip}}</p>
<p>Email: {{$purchase->billing_email}}</p>

<p>Shipping Info:<br/>
    @if($purchase->shipping_street == null)
        {{$purchase->billing_name}}<br/>
        {{$purchase->billing_street}}<br/>
        {{$purchase->billing_city}}, {{$purchase->billing_state}} {{$purchase->billing_zip}}</p>
    @else
        {{ $purchase->shipping_name }}<br/>
        {{ $purchase->shipping_street }}<br/>
        {{ $purchase->shipping_city }}, {{ $purchase->shipping_state }} {{ $purchase->shipping_zip }}</p>
    @endif
<p>You should see the payment in the stripe dashboard at this point!</p>

