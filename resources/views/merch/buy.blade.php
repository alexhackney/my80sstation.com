<!DOCTYPE html>
<html>
<head>
    <title>My 80s - Buy {{$merch->name}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="https://use.typekit.net/hru4bkg.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <style type="text/css">
        img{width: 90%;max-width:540px;margin:0 auto;display:block;}
        img.logo{width: 80%;max-width:200px;margin-bottom:2em;}
        img.item {border:2px black solid;padding:1em;margin-bottom:1em; max-width:200px;}
        p {text-align: center;font-size:1.6rem;font-family: trade-gothic-next, sans-serif;font-weight: 700;font-style: normal;}
        p.small {text-align: center;font-size:1.2rem;font-family: trade-gothic-next, sans-serif;font-weight: 300;font-style: normal;}
        p a {color:orange;text-decoration-line: none;}
        p a:hover {color:purple;text-decoration-line: none;}
        body {width:100%;}
        h1,h2,h3,h4 {font-family: trade-gothic-next, sans-serif; text-align:center;}
        h3 {font-size:1.2em;}
        .go-fund-me {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .go-fund-me h2 {margin-top:1.5em;font-size:3em;}
        .go-fund-me img {width:180px;}
        .blog-posts {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .blog-posts h2 {margin-top:1.5em;font-size:3em;}
        .blog-posts h3 {font-size:2.4em;}
        .entry > a {font-size:1.6em; color:orange;text-decoration: none;}
        .entry > a:hover {color:red;}
        .entry > p {font-size:1.2em;}
        .go-fund-me img {width:200px;}
        .playlist {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .playlist h3 {font-size:1.2em;}
        .playlist p {font-size:1em;}
        .playlist p span {color:orange; font-size:.9em;}
        .preloader {width:100%; height:100%; z-index: 1000;position: fixed;background-color:#27a8e1;}
        .preloader img {margin-top:10%;}
        .btn {
            color: white;
            text-align: center;
            text-decoration: none;
            font-size: 1em;
            font-weight: bold;
            display:block;
        }
        .merch-purch {
            text-align: center;
        }
        .bg-blue {background-color: #29aae22b; padding:1em;}
        .bg-pink {background-color: #ec038c42; padding:1em;}
    </style>
</head>
<body>
<a href="https://my80s.live"><img class="logo" src="/images/my-80s-triangle-logo.png" alt="My 80s" title="My 80s"></a>

<h1>{{ $merch->name }}</h1>
<img src="{{$merch->image_path}}" class="item" alt="Buy a My80s {{$merch->name}}">
<div class="container-fluid">
    <div class="row">
        <div class="col col-md-6 offset-md-3">
            <form class="merch-purch" id="merch-purch" action="{{url('/merch/purchase/submit')}}">
                @csrf
                <input id="merch_id" name="merch_id" value="{{$merch->id}}" hidden>
                <div class="row">
                    <div class="col">
                        @if($merch->options)
                            @foreach(json_decode($merch->options, true) as $optionName => $options)
                                <div class="mb-3">
                                    <h2>Choose Option</h2>
                                    <p class="small">*Required</p>
                                    <label class="form-label" for="{{$optionName}}">Choose a {{ucwords($optionName)}}</label>
                                    <select id="{{$optionName}}" class="form-control" name="{{$optionName}}" required>
                                        <option></option>
                                        @foreach($options as $optionValue => $optionName)
                                        <option value="{{$optionValue}}">{{ucwords($optionName)}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="my-4"></div>
                <div class="container bg-blue">
                    <div class="row">
                        <div class="col">
                            <h2>Billing Information</h2>
                            <p class="small">*Required</p>
                            <div class="mb-3">
                                <label class="form-label" for="billing_name">Billing Name</label>
                                <input id="billing_name" class="form-control" type="text" name="billing_name" placeholder="John Rocker" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="billing_street">Billing Street Address</label>
                                <input id="billing_street" class="form-control" type="text" name="billing_street" placeholder="123 Main St." required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label class="form-label" for="billing_city">Billing City</label>
                                <input id="billing_city" class="form-control" type="text" name="billing_city" placeholder="Ashland" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">

                            <div class="mb-3">
                                <label class="form-label" for="billing_state">Billing State</label>
                                <input id="billing_state" class="form-control" type="text" name="billing_state" placeholder="Ky" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label class="form-label" for="billing_zip">Billing Zipcode</label>
                                <input id="billing_zip" class="form-control" type="text" name="billing_zip" placeholder="41101" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="billing_email">Email</label>
                                <input id="billing_email" class="form-control" type="email" name="billing_email" placeholder="80shairrocker@gmail.com" required>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="shipping-address-btn" class="row my-4">
                    <div class="col my-2">
                        <h2>Buying as a gift?</h2>
                        <a onclick="enableShippingAddress()" class="btn btn-block btn-warning">Ship to Different Address?</a>
                    </div>
                </div>

                <div id="shipping-address" class="container d-none bg-pink">
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="shipping_name">Shipping Name</label>
                                <input id="shipping_name" class="form-control" type="text" name="shipping_name" placeholder="John Rocker">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="mb-3">
                                <label class="form-label" for="shipping_street">Shipping Street Address</label>
                                <input id="shipping_street" class="form-control" type="text" name="shipping_street" placeholder="123 Main St.">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label class="form-label" for="shipping_city">Shipping City</label>
                                <input id="shipping_city" class="form-control" type="text" name="shipping_city" placeholder="Ashland">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">

                            <div class="mb-3">
                                <label class="form-label" for="shipping_state">Shipping State</label>
                                <input id="shipping_state" class="form-control" type="text" name="shipping_state" placeholder="Ky">
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="mb-3">
                                <label class="form-label" for="shipping_zip">Shipping Zipcode</label>
                                <input id="shipping_zip" class="form-control" type="text" name="shipping_zip" placeholder="41101">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row my-4">
                    <div class="col">
                        <h2>Purchase Info</h2>
                        <h3>Item Total: ${{ $merch->price / 100 }}</h3>
                        <h3>Shipping Total: ${{ $merch->shipping / 100 }}</h3>
                        <h3>Total: ${{ ($merch->price + $merch->shipping) / 100 }}</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <h2>Payment Info</h2>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col">
                        <div id="payment-request-button">
                            <!--Wallet Payments-->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <p id="display-card-form-btn" class="btn btn-success btn-block">Buy with a Credit/Debit Card</p>
                    </div>
                </div>
                <div id="card-form" class="row d-none">
                    <div class="col">
                        <h5>Buy with a Credit/Debit Card</h5>
                        <div class="row">
                            <div class="col">
                                @include('purchases.card-form')
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <button type="submit" id="payment-submit-btn" class="btn w-100 btn-success my-1">Submit Credit Card Payment</button>
                                <p class="small">Your order is secured by SSL and Stripe Payments! We do not store any payment information on our server!</p>
                                <p class="small">Please allow 4-6 weeks for delivery!</p>
                            </div>
                        </div>
                        <div id="please-wait-msg" class="row d-none">
                            <div class="col">
                                <p class="alert alert-info text-center">Please Do Not Leave The Page or Refresh. Payment is Processing!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="https://js.stripe.com/v3/"></script>
<script src="/js/app.js"></script>
<script>
    function enableShippingAddress(){
        document.getElementById('shipping-address').classList.remove('d-none');
        document.getElementById('shipping-address-btn').classList.add('d-none');
    }

    const stripe = Stripe('{{env('STRIPE_PUBLIC')}}',
        {
            apiVersion: "2020-08-27",
        });
    const paymentIntent = "{{ $paymentIntent->client_secret }}";
    const paymentLabel = "{{ $paymentIntent->statement_descriptor }} - {{ $paymentIntent->statement_descriptor_suffix }}";
    const paymentAmount = {{ $paymentIntent->amount }};

</script>

<script src="{{ mix('/js/purchase.js') }}"></script>
</body>
</html>
