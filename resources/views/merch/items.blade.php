<div style="
    text-align: center;
    background-color: #636b6f;
    padding: 2em;
    width: 100%;
    color:whitesmoke;
    font-size:2em;
    max-width: 800px;
    margin: 1em auto;
    border-radius: 20px;
    border: 1px solid black;
    box-shadow: 4px 4px 10px #2d3748;"
>
    <h2>We Have Merch!</h2>
    @foreach($merch as $merch)
        <div class="merch-item" style="margin-bottom:4em;">
            <a class="btn" href='#' target="_blank">
                <h3>{{$merch->name}}</h3>
                <img src="{{$merch->image_path}}">
            </a>
            <h4>${{($merch->price / 100)}}</h4>
            <a class="btn" href='#' target="_blank">Buy Now!</a>
        </div>
    @endforeach
</div>
