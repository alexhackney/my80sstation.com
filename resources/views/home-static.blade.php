<!DOCTYPE html>
<html>
<head>
    <title>My 80s</title>
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="https://use.typekit.net/hru4bkg.css">
    <style type="text/css">
        img{width: 90%;max-width:540px;margin:0 auto;display:block;}
        img.logo{width: 80%;max-width:200px;margin-bottom:2em;}
        p {text-align: center;font-size:1.6rem;font-family: trade-gothic-next, sans-serif;font-weight: 700;font-style: normal;}
        p.small {text-align: center;font-size:1.2rem;font-family: trade-gothic-next, sans-serif;font-weight: 300;font-style: normal;}
        p a {color:orange;text-decoration-line: none;}
        p a:hover {color:purple;text-decoration-line: none;}
        h1,h2,h3,h4 {font-family: trade-gothic-next, sans-serif;}
        .go-fund-me {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .go-fund-me h2 {margin-top:1.5em;font-size:3em;}
        .go-fund-me img {width:180px;}
        .blog-posts {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .blog-posts h2 {margin-top:1.5em;font-size:3em;}
        .blog-posts h3 {font-size:2.4em;}
        .entry > a {font-size:1.6em; color:orange;text-decoration: none;}
        .entry > a:hover {color:red;}
        .entry > p {font-size:1.2em;}
        .go-fund-me img {width:200px;}
        .playlist {font-family: trade-gothic-next, sans-serif; text-align:center;}
        .playlist h3 {font-size:1.2em;}
        .playlist p {font-size:1em;}
        .playlist p span {color:orange; font-size:.9em;}
        .preloader {width:100%; height:100%; z-index: 1000;position: fixed;background-color:#27a8e1;}
        .preloader img {margin-top:10%;}
        .alert {
            background-color: #29aae22b; padding:1em;margin:2em;
        }
        .btn {
            background-color: #4CAF50; /* Green */
            border: none;
            color: white;
            padding: 2em 3em;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 20px;
            font-weight: bold;
            border: 2px black solid;
        }
    </style>
</head>
<body>
@if(!session()->has('loader'))
<div id="preloader" class="preloader">
    <img src="my-80s-loader.gif" alt="Loading up the 80s">
</div>

    @php
    Session::put('loader', true);
    @endphp
    @else
    @php
    Session::pull('loader');
    @endphp
@endif
<a href="https://my80s.live"><img class="logo" src="/images/my-80s-triangle-logo.png" alt="My 80s" title="My 80s"></a>
@if(session()->has('order_successful'))
    @php
          $purchase = Session::pull('order_successful');
    @endphp
    <div class="alert">
        <p>We have successfully received your order for {{$purchase['item_name']}}!</p>
        <p>Please allow 4-6 weeks for delivery!</p>
        <p>Thank you for supporting us!</p>
    </div>
@endif
<img class="boombox" src="/images/my-80s-boombox.png" alt="My 80s" title="My 80s" onclick="toggleStream()">

<audio id="stream">
    <source src="https://ice.my80sstation.com/aac" type="audio/aac">
    <source src="https://ice.my80sstation.com/mp3" type="audio/mp3">
</audio>

@if(isset($playlist))
    <div class="playlist">
    <h3>Playlist</h3>
    @foreach($playlist as $song)
            <p class="small">{{ $song->artist }}: {{ $song->title }} <span>{{ $song->played_at }}</span></p>
    @endforeach
    </div>
@endif
@include('coffee')
@if($merch != null)
@include('merch.items')
@endif
<p>Got a comment, a Request?<br/>Let's hear it... <a href="mailto:hello@my80sstation.com" target="_blank">hello@my80sstation.com</a></p>
<p class="small">This website is being built now. Enjoy the music. Should work on any device.</p>
<p class="mt-3"><a href="tel:18778088300">1-877-808-8300</a>
<p class="small">Requests, Contests, Birthday Shout-outs</p>


<p class="small">Trouble? <a href="mailto:admin@my80sstation.com" target="_blank">admin@my80sstation.com</a></p>

<script>
    var myMusic= document.getElementById("stream");
    function toggleStream() {
            myMusic.play();
    }

    document.addEventListener("DOMContentLoaded", function(){
        setTimeout(function() {

            fadeOutPreloader();

        }, 6000);
    });

    function fadeOutPreloader() {
        var fadeTarget = document.getElementById("preloader");
        var fadeEffect = setInterval(function () {
            if (!fadeTarget.style.opacity) {
                fadeTarget.style.opacity = 1;
            }
            if (fadeTarget.style.opacity > 0) {
                fadeTarget.style.opacity -= 0.1;
            } else {
                clearInterval(fadeEffect);
            }
        }, 50);

        fadeTarget.remove();
    }
</script>
</body>
</html>
