<?php

namespace Database\Seeders;

use App\Merch;
use Illuminate\Database\Seeder;

class MerchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $item = Merch::find(1);

        $item->options = [
            'size' => [
                's' => 'small',
                'm' => 'medium',
                'l' => 'large',
                'xl' => 'Extra Large',
                '2xl' => '2X Large',
                '3xl' => '3X Large',
            ]
        ];

        $item->save();
    }
}
