<?php
namespace Database\Seeders;
use App\User;
use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Alex Hackney',
                'email' => 'me@alexhackney.com',
                'role' => 'admin'
            ],
            [
                'name' => 'Mike Nelson',
                'email' => 'mikenelsonradio@gmail.com',
                'role' => 'staff'
            ],
        ];


        foreach ($users as $user) {
            $role = Role::where('name', $user['role'])->first();
            $newUser = User::firstOrNew([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => bcrypt(env('DEFAULT_PASSWORD')),
            ])->save();

            $user = User::where('email', $user['email'])->first();
            $user->roles()->sync($role->id);
        }
    }
}
