<?php
namespace Database\Seeders;

use App\Post;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
            [
            'title' => 'The 1980’s.  A New Human Right is Born.',
            'meta_description' => 'The 1980’s was the “Greatest Era Eva”!  The colors were brighter, the hair was bigger, and the music was louder.  The whole decade was just one big music video.',
            'body' => '<p>The 1980&rsquo;s was the &ldquo;Greatest Era Eva&rdquo;!&nbsp; The colors were brighter, the hair was bigger, and the music was louder.&nbsp; The whole decade was just one big music video.&nbsp; My80s Life and My80s Station invite you to join us as we look back at the music, movies and media that was just plain awesome (&ldquo;Totally Awesome!&rdquo;).&nbsp;</p>
                <p>&nbsp;</p>
                <p>The 1970&rsquo;s were fully phasing out with the change over from Jimmy Carter to Ronald Reagan.&nbsp; A time where Americans would express their rights and desires for a new decade.&nbsp; But for a new generation, who were not even allowed to vote yet, found themselves yelling a new demand that they felt was inalienable right of every American.&nbsp; &nbsp;&nbsp;</p>
                <p>&nbsp;I still remember my friends coming over to my house to hang out and partake in an unlimited stockpile of frozen pizza.&nbsp; We sat there on the couch and watched for the first time &ldquo;Video Killed the Radio Star&rdquo; on MTV.&nbsp; &nbsp;It was like a religious experience for a generation of highly impressionable young people.&nbsp; An explosion of sight and sound.&nbsp; MTV was non-stop music combined with pop culture news.&nbsp; It was like radio 2.0.&nbsp; And it was utterly amazing to know that you were watching Madonna, Michael Jackson and Bon Jovi at the same time as other teenagers all around the nation.&nbsp; An entire generation unified.&nbsp; An entire generation being taught what to wear, how to act and who to listen to.&nbsp; But not all cable companies were carrying MTV at the time.&nbsp; Wait, you mean, there were humans being discriminated against by not having MTV in their lives? As if it were a political right that should be in the Constitution.&nbsp; But that was the influence that MTV had at that time.&nbsp; &nbsp;So, the MTV evangelists Pat Benatar, Billy Idol and Cindy Lauper rallied our generation to stand up in solidarity for those being oppressed of these rights by joining other protestors around the country in saying &ldquo;I Want My MTV&rdquo;.&nbsp; We yelled it at school and at the Mall. We put the bumper stickers on our cars.&nbsp; We wore t-shirts with the words printed out.&nbsp; &nbsp;And Soon every household that had cable had MTV.&nbsp; And All was Good.&nbsp; And together, we sang the lyrics to our generation&rsquo;s Song of Freedom.&nbsp; &nbsp;</p>
                    <p>&ldquo;Around the nation&nbsp;</p>
                    <p>Around the world</p>
                    <p>Everybody have Fun tonight</p>
                    <p>A celebration so spread the word</p>
                    <p>Everybody Wang Chung tonight&rdquo;</p>'
        ]];

        foreach ($posts as $post) {
            Post::updateOrCreate([
                'title' => $post['title'],
                'slug' => Str::slug($post['title']),
                'meta_description' => $post['meta_description'],
                'body' => $post['body'],
            ])->save();
        }
    }
}
